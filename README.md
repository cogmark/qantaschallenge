## QBOT by Mark Graham

#### Introduction

For this code challenge I created a Node.js command line script with a `Bot` class to process commands performed on QBOT.

##### Trade-offs

In the interest of time I didn't structure the codebase as nicely as I would have on a production build, and I'm relatively new to building Node.js applications in particular.

Test coverage could definitely be improved. I tried to cover most 'gotcha' scenarios but some commands (and the `bot-manager` in general) aren't covered by tests.

Would also have liked to use Babel to unlock some newer ES features but for now just kept everything standard.

#### Quick start

Installation:

```
npm install
sudo npm link
./qantas-hotels-robot
```

Run tests:

```
# You might need mocha installed globally.
npm test
```

#### Stack

##### Language used
Node.js v10.12.0

##### Dependencies
chalk, dotenv, mocha, inquirer, shelljs, eslint
