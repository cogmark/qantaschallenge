const assert = require('assert');
const Bot = require('../src/Bot');
const commands = require('../src/utils/command-utils');

describe('Bot', () => {
  describe('Basic functionality', () => {
    it('should construct', () => {
      const TestBot = new Bot(5, 5);
      assert.equal(TestBot.alive, true);
    });

    it('should not be placed by default', () => {
      const TestBot = new Bot(5, 5);
      assert.equal(TestBot.placed, false);
      assert.throws(
        () => {
          throw new Error('Wrong value');
        },
        Error,
      );
    });
  });

  describe('Placing', () => {
    it('should not place with invalid coords', () => {
      let TestBot = null;
      const invalidPlacements = [
        commands.mapCommand('PLACE 5,5,N').options,
        commands.mapCommand('PLACE 16,650,E').options,
        commands.mapCommand('PLACE 2,5,S').options,
        commands.mapCommand('PLACE 5,2,W').options,
      ];

      invalidPlacements.forEach((command) => {
        TestBot = new Bot(5, 5);
        assert.throws(
          () => {
            TestBot.place(command);
          },
          Error,
        );
        assert.equal(TestBot.placed, false);
      });
    });

    it('should place with valid coords', () => {
      let TestBot = null;
      const validPlacements = [
        commands.mapCommand('PLACE 0,0,N').options,
        commands.mapCommand('PLACE 4,4,E').options,
        commands.mapCommand('PLACE 1,2,S').options,
        commands.mapCommand('PLACE 4,0,W').options,
      ];

      validPlacements.forEach((command) => {
        TestBot = new Bot(5, 5);
        assert.equal(typeof TestBot.place(command), 'object');
        assert.equal(TestBot.placed, true);
      });
    });

    it('should be able to place twice', () => {
      const TestBot = new Bot(5, 5);
      TestBot.place(commands.mapCommand('PLACE 0,0,N').options);
      assert.equal(TestBot.x, 0);
      assert.equal(TestBot.y, 0);
      assert.equal(TestBot.f, 'N');

      TestBot.place(commands.mapCommand('PLACE 4,4,S').options);
      assert.equal(TestBot.x, 4);
      assert.equal(TestBot.y, 4);
      assert.equal(TestBot.f, 'S');
    });
  });

  describe('Move - no collisions', () => {
    it('move north with no collision', () => {
      const TestBot = new Bot(5, 5);
      TestBot.place(commands.mapCommand('PLACE 0,3,N').options);

      TestBot.move();
      assert.equal(TestBot.y, 4);
    });

    it('move east with no collision', () => {
      const TestBot = new Bot(5, 5);
      TestBot.place(commands.mapCommand('PLACE 0,3,E').options);

      TestBot.move();
      assert.equal(TestBot.x, 1);
    });

    it('move south with no collision', () => {
      const TestBot = new Bot(5, 5);
      TestBot.place(commands.mapCommand('PLACE 0,3,S').options);

      TestBot.move();
      assert.equal(TestBot.y, 2);
    });

    it('move west with no collision', () => {
      const TestBot = new Bot(5, 5);
      TestBot.place(commands.mapCommand('PLACE 1,3,W').options);

      TestBot.move();
      assert.equal(TestBot.x, 0);
    });
  });

  describe('Move - collisions', () => {
    it('move north with collision', () => {
      const TestBot = new Bot(5, 5);
      TestBot.place(commands.mapCommand('PLACE 0,4,N').options);

      assert.throws(
        () => {
          TestBot.move();
        },
        Error,
      );
      assert.equal(TestBot.y, 4);
    });

    it('move east with collision', () => {
      const TestBot = new Bot(5, 5);
      TestBot.place(commands.mapCommand('PLACE 4,3,E').options);

      assert.throws(
        () => {
          TestBot.move();
        },
        Error,
      );
      assert.equal(TestBot.x, 4);
    });

    it('move south with collision', () => {
      const TestBot = new Bot(5, 5);
      TestBot.place(commands.mapCommand('PLACE 0,0,S').options);

      assert.throws(
        () => {
          TestBot.move();
        },
        Error,
      );
      assert.equal(TestBot.y, 0);
    });

    it('move west with collision', () => {
      const TestBot = new Bot(5, 5);
      TestBot.place(commands.mapCommand('PLACE 0,0,W').options);

      assert.throws(
        () => {
          TestBot.move();
        },
        Error,
      );
      assert.equal(TestBot.x, 0);
    });
  });
});
