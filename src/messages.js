module.exports = () => ({
  initialPrompt: 'Please enter a command for QHBOT:',
  invalid: {
    placement: 'That is not a valid PLACE command. Please use PLACE X,Y,Z (see README.md).',
    bounds: 'QHBOT cannot spawn there! Position is out of bounds of the table.',
    move: 'QHBOT cannot move there! Bot would fall off the table.',
  },
  responses: {
    place: (x, y, z) => `QHBOT successfully placed at (${x},${y}) facing ${z}.`,
    move: (x, y) => `QHBOT moved! Now placed at (${x},${y}).`,
    report: (x, y, f) => `Hello! QHBOT currently at (${x}, ${y}), facing ${f}.`,
    left: f => `QHBOT turned left! Now facing ${f}.`,
    right: f => `QHBOT turned right! Now facing ${f}.`,
  },
});
