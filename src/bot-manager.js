const inquirer = require('inquirer');
const print = require('./utils/print-utils');
const commands = require('./utils/command-utils');
const messages = require('./messages')();
const Bot = require('./Bot');

let ManagedBot = null;
let command = null;

const botCommand = () => inquirer.prompt([{
  name: 'botCommand',
  type: 'input',
  message: messages.initialPrompt,
}]);

const placeBot = async () => {
  const setupOptions = await botCommand();
  command = await commands.mapCommand(setupOptions.botCommand);

  if (command === null || command.match.method !== 'place') {
    print.failure(messages.invalid.placement);
    return false;
  }

  return command.options;
};

const controlBot = async () => {
  while (ManagedBot.alive) {
    const botAction = await botCommand();
    command = await commands.mapCommand(botAction.botCommand);
    if (command !== null) {
      try {
        const response = ManagedBot[command.match.method](command.options);

        print[response.type](response.message);
      } catch (error) {
        print.failure(error);
      }
    }
  }

  print.info(messages.goodbye);
};

module.exports = async () => {
  const confines = process.env.INITIAL_DIMENSIONS.split(',');
  ManagedBot = new Bot(confines[0], confines[1]);
  while (!ManagedBot.placed) {
    const botSettings = await placeBot();
    if (botSettings) {
      try {
        const placement = ManagedBot.place(botSettings);

        print[placement.type](placement.message);
      } catch (error) {
        print.failure(error);
      }
    }
  }

  await controlBot();
};
