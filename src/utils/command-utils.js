const validCommands = [
  {
    method: 'place',
    regex: /PLACE (\d+),(\d+),([NESW])/,
  },
  {
    method: 'report',
    regex: /REPORT/,
  },
  {
    method: 'move',
    regex: /MOVE/,
  },
  {
    method: 'left',
    regex: /LEFT/,
  },
  {
    method: 'right',
    regex: /RIGHT/,
  },
];

const isValidCommand = command => typeof command === 'string';

const findCommand = (command) => {
  const match = validCommands.find(format => command.match(format.regex));

  if (match) {
    return {
      options: command.match(match.regex),
      match,
    };
  }
  return null;
};

module.exports.mapCommand = command => isValidCommand(command) && findCommand(command);
