const chalk = require('chalk');

const successChalk = chalk.green.bgBlack.bold;
const failureChalk = chalk.red.bgBlack.bold;
const infoChalk = chalk.cyan.bgBlack;

module.exports.success = message => console.log(successChalk(`> ${message}`));
module.exports.failure = message => console.log(failureChalk(`> ${message}`));
module.exports.info = message => console.log(infoChalk(`> ${message}`));
