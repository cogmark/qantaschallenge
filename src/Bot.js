const messages = require('./messages')();

const directions = ['N', 'E', 'S', 'W'];
const directionModifiers = {
  N: ['y', 1],
  E: ['x', 1],
  S: ['y', -1],
  W: ['x', -1],
};

module.exports = class Bot {
  constructor(confineX, confineY) {
    this.confines = {
      x: {
        low: 0,
        high: confineX - 1,
      },
      y: {
        low: 0,
        high: confineY - 1,
      },
    };
    this.alive = true;
    this.placed = false;
  }

  validPosition(x, y) {
    return x >= this.confines.x.low
      && x <= this.confines.x.high
      && y >= this.confines.y.low
      && y <= this.confines.y.high;
  }

  directionId() {
    return directions.findIndex(direction => direction === this.f);
  }

  changePlace(x, y) {
    this.x = parseInt(x, 10);
    this.y = parseInt(y, 10);
  }

  changeDirection(direction) {
    this.f = direction;
  }

  modifyPlace(modifier) {
    const axis = modifier[0];
    const adjustment = modifier[1];
    return {
      x: axis === 'x' ? this.x + adjustment : this.x,
      y: axis === 'y' ? this.y + adjustment : this.y,
    };
  }

  placeBot(x, y, f) {
    this.changePlace(x, y);
    this.changeDirection(f);
    this.placed = true;
  }

  place(options) {
    const x = options[1];
    const y = options[2];
    const f = options[3];

    if (!this.validPosition(x, y)) {
      throw new Error(messages.invalid.bounds);
    }

    this.placeBot(x, y, f);
    return {
      type: 'success',
      message: messages.responses.place(x, y, f),
    };
  }

  move() {
    const modifier = directionModifiers[this.f];
    const proposedPlace = this.modifyPlace(modifier);
    const { x, y } = proposedPlace;

    if (!this.validPosition(x, y)) {
      throw new Error(messages.invalid.move);
    }

    this.changePlace(x, y);
    return {
      type: 'success',
      message: messages.responses.move(x, y),
    };
  }

  left() {
    this.changeDirection(this.f === 'N'
      ? 'W'
      : directions[this.directionId() - 1]);

    return {
      type: 'success',
      message: messages.responses.left(this.f),
    };
  }

  right() {
    this.changeDirection(this.f === 'W'
      ? 'N'
      : directions[this.directionId() + 1]);

    return {
      type: 'success',
      message: messages.responses.right(this.f),
    };
  }

  report() {
    return {
      type: 'info',
      message: messages.responses.report(this.x, this.y, this.f),
    };
  }
};
